<?php
/*
    Plugin Name:    Lexicon IT-Konsult: WordPress Test 1
    Description:    WordPress Test 1
    Author:         Lexicon IT-Konsult
    Version:        1.0
*/

include(plugin_dir_path(__FILE__) . 'class/class-custom-post.php');
include(plugin_dir_path(__FILE__) . 'class/class-widget.php');

class LexiconWpTest1
{
    protected $_servicesCustomPostType;
    protected $_servicesWidget;

    public function __construct()
    {
        // Creates and hold an instance of the LexiconWpTest1CustomPost class
        $this->_servicesCustomPostType = new LexiconWpTest1CustomPost();

        // Creates and hold an instance of the LexiconTest1Widget class
        $this->_servicesWidget = new LexiconWpTest1Widget();
    }
}

/**
 * Create and hold an instance of the LexiconWpRest1 in the GLOBALS
 * @var $GLOBALS['lexiconWpTest1']
 */
$GLOBALS['lexiconWpTest1'] = new LexiconWpTest1();
