<?php

/**
 * Widget for our custom post types
 */
class LexiconWpTest1Widget extends WP_Widget
{

    protected $_viewsPath;

    /**
     * Registers the LexiconWpTest1Widget class on the "widgets_init" hook
     */
    public function __construct()
    {
        $this->_viewsPath = plugin_dir_path(plugin_dir_path(__FILE__)) . 'view/';

        add_action('widgets_init', array($this, 'registerLexiconTestWidget'));

        parent::__construct(
            'LexiconWpTest1Widget', // ID of the widget
            __('List of services'), // Name of the widget
            array(
                'description' => __('Listing services from the "Lexicon IT-Konsult: WordPress Test 1" plugin')
            )
        );
    }

    /**
     * Registers the widget
     */
    public function registerLexiconTestWidget()
    {
        register_widget('LexiconWpTest1Widget');
    }

    /**
     * Creates the form in the widgets screen
     * @param array $instance The widget options
     */
    public function form($instance)
    {
        /**
         * Holds list of default values for the instance settings
         * @var array
         */
        $defaults = array(
            'title'    => 'List of services',
            'numPosts' => 0
        );

        /**
         * Merges the $defaults with the $insatance options
         * @var array
         */
        $instance = array_merge($defaults, $instance);

        // Require the widget form view
        require($this->_viewsPath . 'view-widget-form.php');
    }

    /**
     * Prepare widget options for save
     * @param  array $newInstance The new widget options
     * @param  array $oldInstance The old widget options
     * @return array              The merged instande of new and old to be saved
     */
    public function update($newInstance, $oldInstance)
    {
        /**
         * Merges $oldInstance and $newInstance
         * @var array
         */
        $instance = array_merge($oldInstance, $newInstance);

        // Validates numPosts option
        if (empty($instance['numPosts'])) {
            $instance['numPosts'] = 0;
        }

        return $instance;
    }

    /**
     * The output of the widget to the frontend
     * @param  array $args     The settings of the widget
     * @param  array $instance The widget instance
     */
    public function widget($args, $instance)
    {
        /**
         * The arguments to use in the WP_Query
         * @var array
         */
        $queryArgs = array(
            'post_type'      => 'services',
            'posts_per_page' => $instance['numPosts']
        );

        /**
         * Get the posts
         * @var WP_Query
         */
        $services = new WP_Query($queryArgs);

        /**
         *  Checks if there's posts to display or not
         */
        if ($services->have_posts()) {
            // Require the widget form view to display the posts
            require($this->_viewsPath . 'view-widget.php');
        } else {
            echo __('No services to display.');
        }
    }
}
