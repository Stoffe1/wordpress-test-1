<?php

/**
 * Our custom post type
 */
class LexiconWpTest1CustomPost
{
    /**
     * Registers the custom post type on the "init" hook
     */
    public function __construct()
    {
        add_action('init', array($this, 'registerServicesPostType'));
    }

    /**
     * Registers the Lexicon custom post type
     */
    public function registerServicesPostType()
    {
        $labels = array(
            'name'               => _x('Services', 'post type name'),
            'singular_name'      => _x('service', 'post type singular name'),
            'menu_name'          => __('Services'),
            'add_new'            => __('Add new'),
            'add_new_item'       => __('Add new service'),
            'edit_item'          => __('Edit service'),
            'new_item'           => __('New service'),
            'all_items'          => __('All services'),
            'view_item'          => __('View service'),
            'search_items'       => __('Search services'),
            'not_found'          => __('No services found'),
            'not_found_in_trash' => __('No services found in the trash'),
        );

        $args = array(
            'labels' => $labels,
            'description' => 'Services offered by Lexicon IT',
            'public' => true,
            'menu_position' => 100,
            'supports' => array('title', 'editor', 'thumbnail', 'author'),
            'has_archive' => true
        );

        register_post_type('services', $args);
    }
}
