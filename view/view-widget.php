<?php echo $args['before_widget']; ?>
<?php echo $args['before_title']; ?><?php echo $instance['title']; ?><?php echo $args['after_title']; ?>

<?php while ($services->have_posts()) : $services->the_post(); ?>
<div class="service">
	<h1><a href="<?php echo the_permalink(); ?>" title="<?php echo the_title(); ?>"><?php echo the_title(); ?></a></h1>
	<?php the_post_thumbnail() ?>
	<?php the_excerpt(); ?>
</div>
<?php endwhile; ?>

<?php echo $args['after_widget']; ?>