<p>
	<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title'); ?>:</label>
	<input type="text" name="<?php echo $this->get_field_name('title'); ?>" id="<?php echo $this->get_field_id('title'); ?>" class="widefat" value="<?php echo $instance['title']; ?>">
</p>
<p>
	<label for="<?php echo $this->get_field_id('numPosts'); ?>"><?php _e('Number of posts to show'); ?>:</label>
	<input type="number" name="<?php echo $this->get_field_name('numPosts'); ?>" id="<?php echo $this->get_field_id('numPosts'); ?>" class="widefat" value="<?php echo $instance['numPosts']; ?>">
</p>